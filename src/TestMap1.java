public class TestMap1 {
    private String name;
    private double height;
    private double width;

    public TestMap1(String name, double height, double width) {
        this.name = name;
        this.height = height;
        this.width = width;
    }

    public void printMap() {
        for(int i=0; i<height; i++) {
            for(int j=0; j<width; j++) {
                System.out.print("-");
            }
            System.out.print("\n");
        }
    }
    public static void main(String[] args) {
        TestMap1 testmap1 = new TestMap1("testmap1", 5, 5);
        testmap1.printMap();
    }
}
