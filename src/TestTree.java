public class TestTree {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public static final int MIN_X =0;
    public static final int MIN_Y =0;
    public static final int MAX_X =19;
    public static final int MAX_Y =19;
    public static final Object Y_MIN = null;

    public TestTree(String name, char symbol, int x, int y) {
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }

    public TestTree(String name , char symbol) {
        this(name , symbol , 0, 0);

    }

    public void print() {
        System.out.println("TestTree: " + name + " X:" + x + " Y: " + y);
    }
    public void setName(String name) {
        this.name =name;
    }
    public String getName() {
        return name;
    }
    public char getSymbol() {
        return symbol;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}
