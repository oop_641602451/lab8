import java.util.Scanner;

public class TestMap2 {
    private String name;
    private double height;
    private double width;

    public TestMap2(String name, double height, double width) {
        this.name = name;
        this.height = height;
        this.width = width;
    }

    public void printMap() {
        for(int i=0; i<height; i++) {
            for(int j=0; j<width; j++) {
                System.out.print("-");
            }
            System.out.print("\n");
        }
    }
    public static void main(String[] args) {
        TestMap2 testmap2 = new TestMap2("testmap2", 10, 10);
        testmap2.printMap();
    }
}
