public class circle {
    private String name;
    private int radius;

    public circle(String name, int radius) {
        this.name = name;
        this.radius = radius;
    }
    public void printRectArea(){
        System.out.println(name + "=" + (3.14*radius*radius));
    }
    public void printPerimeter(){
        System.out.println(name + "=" + 2*3.14*radius);
    }
}
