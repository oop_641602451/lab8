public class TestTree1 {
    public static void main(String[] args) {
        TestTree Tree01 = new TestTree("Tree01", 'T', 5, 10) ;
        Tree01.print();
        TestTree Tree02 = new TestTree("Tree02", 't', 5, 11) ;
        Tree02.print();

        for(int y = TestTree.MIN_Y; y<TestTree.MAX_Y; y++) {
            for(int x = TestTree.MIN_Y; x<TestTree.MAX_Y; x++) {
                if(Tree01.getX() == x && Tree01.getY() == y) {
                System.out.print(Tree01.getSymbol()); 
                } else if(Tree02.getX() == x && Tree02.getY() == y){
                    System.out.print(Tree02.getSymbol());
                } else {
                    System.out.print("-");
                }
            }
            System.out.println();
        }
    }
}
